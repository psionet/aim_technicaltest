﻿var viewModel = {
    url: ko.observable("http://harry.radioapi.io/services/nowplaying/fm104/fm104/onair"),
    list: ko.observableArray([]),
    updateFeed: function () {
        $.ajax({
            type: 'GET',
            url: "/api/DataFeed/", 
            data: {data: this.url()},
            success: function (data) {
                viewModel.list(mapResults(data.playoutItem));
                viewModel.lastAjaxStatus(true);
            },
            error: function () {
                viewModel.list(ko.observableArray([]));
                viewModel.lastAjaxStatus(false);
            }
        });
    },
    now: ko.observable(moment()),
    lastAjaxStatus: ko.observable(true)    
};
ko.applyBindings(viewModel);

//Stop the render flash before the knockout has loaded....
$("#app").css("visibility", "visible");

function mapResults(data) {
    return ko.utils.arrayMap(data, function (item) {
        var mapped = ko.mapping.fromJS(item);

        mapped.itemLocalTime = ko.computed(function () {
            return moment(moment.utc(item.time).toDate());
        });

        mapped.duration = ko.computed(function () {
            return moment.duration(item.duration);
        });

        mapped.formattedDuration = ko.observable(Math.floor(mapped.duration().asHours()) + moment.utc(mapped.duration().asMilliseconds()).format(":mm:ss"));
        
        mapped.currentlyPlaying = ko.computed(function () {
            if (item.status === "playing") {
                return viewModel.now().isBefore(moment(moment.utc(item.time).add(mapped.duration()).toDate()));
            }
            return false;
        }, this);
        
        mapped.progressPercentage = ko.computed(function () {
            if (mapped.currentlyPlaying()) {
                
                var x = viewModel.now().diff(mapped.itemLocalTime()); //Should be +ve value
                var y = x / mapped.duration() * 100;
                return y + "%";
            }
            return "100%";
        }, this);

        mapped.progress = ko.computed(function () {
            if (mapped.currentlyPlaying()) {
                return moment.utc(viewModel.now().diff(mapped.itemLocalTime())).format("mm:ss") + " / " + mapped.formattedDuration();
            }
            return 0;
        }, this);
    
        return mapped;
    })
}

setInterval(function () {
    viewModel.now(moment());
}, 1000);





