﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AIM_Test.Models
{
    public partial class onAir
    {
        public void validateAllImageLinksAsync()
        {
            Parallel.ForEach(this.playoutItemField, x => x.validateUrl());
        }
    }

    public partial class onAirPlayoutItem
    {
        public bool ImageLinkBad { get; set; }

        public void validateUrl()
        {
            var request = HttpWebRequest.Create(this.imageUrl);
            request.Method = "HEAD";
            using (HttpWebResponse wr = (HttpWebResponse)request.GetResponse())
            {
                this.ImageLinkBad = wr.StatusCode != HttpStatusCode.OK;
            }
        }
    }
}
