﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml.Serialization;


namespace AIM_Test.Controllers
{
    public class DataFeedController : ApiController
    {
        public IHttpActionResult Get(string data)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(data);
                  //Request text/xml even though that's all the endpoint serves. That way we don't need to check the response to parse multuple types inf this test (we normally would)
                request.Accept = "text/xml";
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        XmlSerializer x = new XmlSerializer(typeof(Models.onAir));
                        AIM_Test.Models.onAir onAir = (AIM_Test.Models.onAir)x.Deserialize(response.GetResponseStream());
                        onAir.validateAllImageLinksAsync();
                        return Ok(onAir);
                    }
                }
            }
            catch
            { 
                //Eats Exceptions... Not implementing error handling for test.
                return StatusCode(HttpStatusCode.InternalServerError);
            }
            return NotFound();
        }

        

    }
}
